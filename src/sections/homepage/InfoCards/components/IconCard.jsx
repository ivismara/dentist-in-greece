import React from "react";
import { Card, makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
    textAlign: "center",
    backgroundColor: "#ffffff",
    height: 240,
  },
  icon: {
    maxWidth: 40,
  },
}));

const IconCard = ({ icon, title, text, anchor }) => {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <img src={icon} className={classes.icon} />
      <Typography
        variant="h6"
        component="p"
        color="primary"
        style={{ marginTop: 10 }}
      >
        {title}
      </Typography>
      <Typography variant="body1" component="p">
        {text}
      </Typography>
    </Card>
  );
};

export default IconCard;
