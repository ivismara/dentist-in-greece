import React from "react";
import TestIcon from "../../../images/logo.svg";
import IconCard from "./components/IconCard";
import { makeStyles, Grid } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
  },
  content: {
    position: "absolute",
    top: -40,
    left: 0,
    padding: "0px 120px",
  },
}));

const cards = [
  {
    title: "Lorem Ipsum",
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lorem nibh, accumsan at placerat quis, iaculis quis dolor.",
    icon: TestIcon,
    anchor: "#",
  },
  {
    title: "Lorem Ipsum",
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lorem nibh, accumsan at placerat quis, iaculis quis dolor.",
    icon: TestIcon,
    anchor: "#",
  },
  {
    title: "Lorem Ipsum",
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lorem nibh, accumsan at placerat quis, iaculis quis dolor.",
    icon: TestIcon,
    anchor: "#",
  },
];

const InfoCards = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <Grid container spacing={5}>
          {cards.map((card) => (
            <Grid item sm={4}>
              <IconCard {...card} />
            </Grid>
          ))}
        </Grid>
      </div>
    </div>
  );
};

export default InfoCards;
