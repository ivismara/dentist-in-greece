import React from "react";
import { Grid, makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.primary.dark,
    padding: 60,
    color: "#ffffff",
  },
  link: {
    color: theme.palette.primary.contrastText,
    textDecoration: "none",
    fontWeight: 300,
  },
}));

const Footer = () => {
  const classes = useStyles();

  return (
    <Grid container className={classes.root}>
      <Grid item sm={6}></Grid>
      <Grid item sm={3}>
        <Typography
          variant="body1"
          component="p"
          color="inherit"
          style={{ fontWeight: 700 }}
        >
          Andriolo Omar
        </Typography>
        <Typography variant="body2" component="p" color="inherit">
          <a href="tel: +39 334 7646608" className={classes.link}>
            +39 334 7646608
          </a>
        </Typography>
        <Typography variant="body2" component="p" color="inherit">
          <a href="mailto: dentistingreece@gmail.com" className={classes.link}>
            dentistingreece@gmail.com
          </a>
        </Typography>
      </Grid>
      <Grid item sm={3}>
        <Typography
          variant="body1"
          component="p"
          color="inherit"
          style={{ fontWeight: 700 }}
        >
          Clinica Kalliston
        </Typography>
        <Typography
          variant="body2"
          component="p"
          color="inherit"
          style={{ fontWeight: 300 }}
        >
          Vasilikis Tavaki, 36
        </Typography>
        <Typography
          variant="body2"
          component="p"
          color="inherit"
          style={{ fontWeight: 300 }}
        >
          57001 Thermi
        </Typography>
        <Typography
          variant="body2"
          component="p"
          color="inherit"
          style={{ fontWeight: 300 }}
        >
          Thessaloniki - Greece
        </Typography>
        <Typography variant="body2" component="p" color="inherit">
          <a href="tel: +30 2310 478004" className={classes.link}>
            +30 2310 478004
          </a>
        </Typography>
      </Grid>
    </Grid>
  );
};

export default Footer;
