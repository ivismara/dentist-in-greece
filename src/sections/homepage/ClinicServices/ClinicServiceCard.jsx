import React from "react";
import { makeStyles, Typography, useTheme } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.primary.contrastText,
    marginLeft: 15,
    marginRight: 15,
  },
  dot: {
    width: 12,
    borderRadius: 50,
    marginRight: 10,
    height: 12,
  },
}));

const ClinicServiceCard = ({ name, type }) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <div className={classes.root}>
      <div
        className={classes.dot}
        style={{
          backgroundColor:
            type === "d"
              ? theme.palette.secondary.main
              : theme.palette.secondary.contrastText,
        }}
      />
      <Typography
        variant="h6"
        color={"inherit"}
        component="p"
        style={{ marginBottom: 0, fontWeight: 400 }}
      >
        {name}
      </Typography>
    </div>
  );
};

export default ClinicServiceCard;
