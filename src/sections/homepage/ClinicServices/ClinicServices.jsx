import React from "react";
import Section from "../../../components/Section";
import { makeStyles, Typography, useTheme } from "@material-ui/core";
import ClinicServiceCard from "./ClinicServiceCard";

const services = [
  { name: "Odontoiatria conservativa", type: "d" },
  { name: "Igiene ed odontoiatria preventiva", type: "d" },
  { name: "Endodonzia", type: "d" },
  { name: "Odontoiatria pediatrica e pedodonzia", type: "d" },
  { name: "Ortodonzia", type: "d" },
  { name: "Protesi dentaria", type: "d" },
  { name: "Implantologia", type: "d" },
  { name: "Parodontologia", type: "d" },
  { name: "Medicina estetica dei tessuti perforati e del volto", type: "e" },
];

const useStyles = makeStyles((theme) => ({
  listContainer: {
    display: "flex",
    justifyContent: "center",
  },
  list: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexWrap: "wrap",
    maxWidth: "60vw",
  },
}));

const ClinicServices = () => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Section dark isLast>
      <Typography
        variant="h3"
        component="h2"
        style={{
          marginBottom: 30,
          color: theme.palette.primary.contrastText,
          textAlign: "center",
        }}
      >
        I servizi della clinica
      </Typography>
      <div className={classes.listContainer}>
        <div className={classes.list}>
          {services.map((s) => (
            <ClinicServiceCard {...s} />
          ))}
        </div>
      </div>
    </Section>
  );
};

export default ClinicServices;
