import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import { makeStyles, Typography, Grid, Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    paddingLeft: 60,
    paddingRight: 60,
    paddingTop: 60,
    paddingBottom: 90,
  },
  backdrop: {
    background: "rgba(255,255,255,0.5)",
    position: "absolute",
    top: 0,
    left: 0,
    height: "100%",
    width: "100%",
  },
}));

const Header = () => {
  const classes = useStyles();
  const image = useStaticQuery(graphql`
    query {
      bg: file(relativePath: { eq: "header.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1920, quality: 100) {
            srcWebp
          }
        }
      }
    }
  `);
  return (
    <Grid
      className={classes.root}
      container
      style={{
        backgroundImage: `url(${image.bg.childImageSharp.fluid.srcWebp})`,
      }}
    >
      <div className={classes.backdrop} />
      <Grid item sm={5} style={{ zIndex: 10 }}>
        <Typography variant="h6" component="h3" color="secondary">
          Trasforma il tuo problema in un'esperienza speciale.
        </Typography>
        <Typography variant="h1" component="h1" style={{ marginBottom: 20 }}>
          Ritorna a sorridere con noi!
        </Typography>
        <Typography variant="body1" component="p" color="primary">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vitae
          elit quis est accumsan vulputate eget in metus. Suspendisse interdum
          nec orci non vehicula. Praesent luctus, ipsum at luctus pellentesque,
          magna arcu scelerisque eros, ac dictum massa nisl in tellus.
        </Typography>
        <Button
          disableElevation
          variant="contained"
          color={"secondary"}
          style={{ marginTop: 40 }}
          size={"large"}
        >
          Scopri di più
        </Button>
      </Grid>
    </Grid>
  );
};

export default Header;
