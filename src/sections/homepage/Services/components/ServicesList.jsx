import React from "react";
import { Grid, makeStyles, Typography } from "@material-ui/core";
import { Check } from "react-feather";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 20,
  },
  service: {
    display: "flex",
    alignItems: "center",
    marginBottom: 10,
  },
  iconContainer: {
    width: 30,
    height: 30,
    backgroundColor: theme.palette.tertiary.main,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: "100%",
  },
}));

const services = [
  {
    name: "Traduzione in lingua italiana terapia/preventivo",
  },
  {
    name: "Pianificazione voli",
  },
  {
    name: "Prenotazione soggiorno",
  },
  {
    name: "Servizio navetta areoporto/hotel - hotel/aeroporto",
  },
  {
    name: "Trasporto hotel/clinica - clinica/hotel",
  },
  {
    name: "Affiancamento e traduzione presso la clinica",
  },
  {
    name: "Supporto legale a tutela del cliente",
  },
  {
    name: "Tanti consigli per passare al meglio il soggiorno!",
  },
];

const ServicesList = () => {
  const classes = useStyles();
  return (
    <Grid container className={classes.root}>
      {services.map((s) => (
        <Grid item sm={6} className={classes.service}>
          <div className={classes.iconContainer}>
            <Check style={{ color: "white" }} size={20} />
          </div>
          <Typography
            component="p"
            variant="body1"
            style={{
              marginLeft: 10,
              flex: 1,
              marginBottom: 0,
              fontWeight: 500,
            }}
          >
            {s.name}
          </Typography>
        </Grid>
      ))}
    </Grid>
  );
};

export default ServicesList;
