import React from "react";
import Section from "../../../components/Section";
import SectionTitle from "../../../components/SectionTitle";
import { Grid, makeStyles, Typography } from "@material-ui/core";
import ServicesList from "./components/ServicesList";
import { graphql, useStaticQuery } from "gatsby";
import FluidImage from "../../../components/FluidImage";

const useStyles = makeStyles((theme) => ({
  root: { marginTop: 200 },
}));

const Services = () => {
  const image = useStaticQuery(graphql`
    query {
      bg: file(relativePath: { eq: "services.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 900, quality: 100) {
            srcWebp
          }
        }
      }
    }
  `);
  const classes = useStyles();
  return (
    <Section>
      <Grid container className={classes.root} spacing={8}>
        <Grid item sm={6}>
          <FluidImage image={image} />
        </Grid>
        <Grid item sm={6}>
          <SectionTitle
            title="Lorem Ipsum"
            subtitle="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
            light
          />
          <Typography
            variant="body1"
            component="p"
            color="primary"
            style={{ fontWeight: 500 }}
          >
            Miriamo alla piena soddisfazione del cliente dall'inzio del viaggio
            fino al suo nuovo sorriso.
          </Typography>
          <Typography variant="body1" component="p">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lorem
            nibh, accumsan at placerat quis, iaculis quis dolor.
          </Typography>
          <ServicesList />
        </Grid>
      </Grid>
    </Section>
  );
};

export default Services;
