import React from "react";
import Section from "../../../components/Section";
import { Grid, Typography } from "@material-ui/core";
import FluidImage from "../../../components/FluidImage";
import { graphql, useStaticQuery } from "gatsby";
import SectionTitle from "../../../components/SectionTitle";

const WhyGreece = () => {
  const image = useStaticQuery(graphql`
    query {
      bg: file(relativePath: { eq: "whygreece.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 900, quality: 100) {
            srcWebp
          }
        }
      }
    }
  `);
  return (
    <Section isLast>
      <Grid container spacing={8}>
        <Grid item sm={6}>
          <SectionTitle
            title="Perché in Grecia"
            light
            subtitle="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
          />
          <Typography variant="body1" component="p">
            La Grecia è un'ottima soluzione per svariati motivi. Il principale
            di questi nel nostro caso, è Il risparmio economico che però non
            deve assolutamente incidere sulla qualità dei materiali e delle
            terapie{" "}
            <strong> risparmiando un 40/50% rispetto ai prezzi italiani</strong>
            .
          </Typography>
          <Typography variant="body1" component="p">
            Essendo la Grecia un Paese membro dell'Unione Europea, a differenza
            dei Paesi dell'Est deve sottostare a{" "}
            <strong>
              tutti i rigidi standard igienici ed i protocolli sanitari vigenti
              all'interno dell'Unione stessa
            </strong>
            .
          </Typography>
          <Typography variant="body1" component="p">
            Altro punto importante è la preparazione professionale dei medici.
            Le{" "}
            <strong>Università Greche sono ad oggi fra le più rinomate</strong>{" "}
            ed i medici Ellenici fra i più richiesti in Europa.
          </Typography>
          <Typography variant="body1" component="p">
            Potrai inoltre conciliare le tue cure con una bella vacanza,
            Salonicco è una bellissima città costiera ricca di attrazioni
            storiche e si trova a pochi chilometri dalle bellissime spiagge
            della Calcidica, una delle migliori mete turistiche della penisola
            Ellenica.
          </Typography>
        </Grid>
        <Grid item sm={6}>
          <FluidImage image={image} />
        </Grid>
      </Grid>
    </Section>
  );
};
export default WhyGreece;
