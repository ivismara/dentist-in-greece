import React from "react";
import { makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: 5,
    height: 100,
    display: "flex",
    alignItems: "center",
    background: theme.palette.background.paper,
    boxShadow: "5px 5px 60px #d0d0d0",
    padding: "10px 40px",
    textDecoration: "none",
    color: theme.palette.secondary.main,
    transition: "all .3s ease",
    "&:hover": {
      transition: "all .3s ease",
      background: theme.palette.secondary.main,
      color: theme.palette.secondary.contrastText,
    },
  },

  icon: {
    color: "inherit",
  },

  text: {
    paddingLeft: 20,
  },
}));

const ContactCard = ({ title, href, icon, text }) => {
  const classes = useStyles();
  const Icon = icon;
  return (
    <a className={classes.root} href={href}>
      <Icon size={30} className={classes.icon} />
      <div className={classes.text}>
        <Typography variant="h6" component="p" color="primary">
          {title}
        </Typography>
        <Typography
          variant="body1"
          component="p"
          color="primary"
          style={{ marginBottom: 0 }}
        >
          {text}
        </Typography>
      </div>
    </a>
  );
};

export default ContactCard;
