import React from "react";
import Section from "../../../components/Section";
import { Grid, Typography } from "@material-ui/core";
import { Facebook, Mail, Phone } from "react-feather";
import ContactCard from "./components/ContactCard";

const contacts = [
  {
    title: "Email",
    icon: Mail,
    text: "dentistingreece@gmail.com",
    href: "mailto:dentistingreece@gmail.com",
  },
  {
    title: "Telefono",
    icon: Phone,
    text: "+39 334 7646608",
    href: "tel:+39 334 7646608",
  },
  {
    title: "Facebook",
    icon: Facebook,
    text: "Omar Andriolo",
    href: "#",
  },
];

const Contact = () => {
  return (
    <Section isLast>
      <Typography
        variant="h3"
        component="h2"
        color="primary"
        style={{
          marginBottom: 30,
          textAlign: "center",
        }}
      >
        Contattami
      </Typography>
      <Typography
        variant="body1"
        component="p"
        style={{ textAlign: "center", marginBottom: 40 }}
      >
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lorem
        nibh, accumsan at placerat quis, iaculis quis dolor.
      </Typography>
      <Grid container spacing={8}>
        {contacts.map((c) => (
          <Grid item sm={4}>
            <ContactCard {...c} />
          </Grid>
        ))}
      </Grid>
    </Section>
  );
};

export default Contact;
