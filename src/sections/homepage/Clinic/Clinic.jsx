import React, { useState } from "react";
import Section from "../../../components/Section";
import SectionTitle from "../../../components/SectionTitle";
import { graphql, useStaticQuery } from "gatsby";
import { Button, Grid, makeStyles } from "@material-ui/core";
import FluidImage from "../../../components/FluidImage";
import ClinicCard from "./components/ClinicCard";
import Clinic1 from "../../../images/clinic-1.svg";
import Clinic2 from "../../../images/clinic-2.svg";
import Clinic3 from "../../../images/clinic-3.svg";
import { Video } from "react-feather";
import ClinicVideoDialog from "./components/ClinicVideoDialog";

const useStyles = makeStyles((theme) => ({}));

const Clinic = () => {
  const [dialog, setDialog] = useState(false);
  const classes = useStyles();
  const image = useStaticQuery(graphql`
    query {
      bg: file(relativePath: { eq: "clinic1.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 900, quality: 100) {
            srcWebp
          }
        }
      }
    }
  `);
  return (
    <Section dark isFirst>
      <Grid container spacing={8}>
        <Grid item sm={6}>
          <FluidImage image={image} />
        </Grid>
        <Grid item sm={6}>
          <SectionTitle
            title="La clinica"
            subtitle="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
          />
          <ClinicCard
            icon={Clinic1}
            text="Situata alle porte di Salonicco, nel quartiere di Thermi ed
              inaugurato a Gennaio 2020 il Policlinico “Kalliston” si propone
              come una delle cliniche più attrezzate e tecnologiche del Nord
              della Grecia."
          />
          <ClinicCard
            icon={Clinic2}
            style={{ marginTop: 20, marginBottom: 20 }}
            text="Punto forte di Kalliston è la presenza di un laboratorio
              odontotencnico dotato di strumentazioni molto sofisticate quali
              Scanner dentale, stampante 3D, CAD CAM per fresatura Zirconio.
              Questo permette di abbattere notevolmente i costi, in quanto la
              clinica non deve appoggiarsi ad un laboratorio esterno e di poter
              velocizzare il processo di produzione degli impianti dentali
              rispettando così le tempistiche programmate."
          />
          <ClinicCard
            icon={Clinic3}
            text="Tutto il personale presente all'interno della clinica detiene una preparazione professionale pluriennale nell’ambitodella propria
specializzazione"
          />
          <Button
            variant="contained"
            size="large"
            color="secondary"
            startIcon={<Video />}
            style={{ marginTop: 40 }}
            onClick={() => setDialog(true)}
          >
            Fai un tour virtuale della clinica
          </Button>
        </Grid>
      </Grid>
      <ClinicVideoDialog open={dialog} onClose={() => setDialog(false)} />
    </Section>
  );
};

export default Clinic;
