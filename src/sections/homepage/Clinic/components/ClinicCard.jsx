import React from "react";
import { Grid, makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  card: {
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: theme.palette.primary.light,
    padding: 20,
    color: theme.palette.primary.contrastText,
    display: "flex",
  },
  icon: {
    marginRight: 20,
    maxHeight: 40,
  },
}));

const ClinicCard = ({ text, style, icon }) => {
  const classes = useStyles();
  return (
    <div className={classes.card} style={style}>
      <img src={icon} className={classes.icon} />
      <Typography
        color="inherit"
        style={{ marginBottom: 0 }}
        variant="body1"
        component="p"
      >
        {text}
      </Typography>
    </div>
  );
};

export default ClinicCard;
