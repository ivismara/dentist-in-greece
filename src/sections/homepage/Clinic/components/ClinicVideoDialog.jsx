import React from "react";
import ReactPlayer from "react-player";
import {
  Dialog,
  DialogContent,
  IconButton,
  makeStyles,
  Typography,
} from "@material-ui/core";
import Video from "../../../../images/clinic.mp4";
import { X } from "react-feather";

const useStyles = makeStyles((theme) => ({
  title: {
    padding: "12px 24px",
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
}));

const ClinicVideoDialog = ({ open, onClose }) => {
  const classes = useStyles();
  return (
    <Dialog open={open} onClose={() => onClose()} fullWidth maxWidth="md">
      <div className={classes.title}>
        <Typography component="h5" variant="h5">
          Tour virtuale della clinica
        </Typography>
        <IconButton onClick={() => onClose()}>
          <X />
        </IconButton>
      </div>
      <DialogContent>
        <ReactPlayer
          url={Video}
          controls
          width={"100%"}
          height={"100%"}
          volume={0.3}
        />
      </DialogContent>
    </Dialog>
  );
};

export default ClinicVideoDialog;
