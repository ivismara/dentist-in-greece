import React from "react";
import Section from "../../../components/Section";
import SectionTitle from "../../../components/SectionTitle";
import { Grid, Typography } from "@material-ui/core";

const WhoAmI = () => {
  return (
    <Section>
      <Grid container spacing={8}>
        <Grid item sm={6}>
          <SectionTitle title="Chi sono" light />
          <Typography variant="body1" component="p">
            Il mio nome è Omar Andriolo, sono nato a Milano nel lontano 1972 e
            nel 2001 mi sono trasferito in Grecia, nello specifico nella città
            di Salonicco dove tutt'ora lavoro e vivo con la mia famiglia.
          </Typography>
          <Typography variant="body1" component="p">
            Non sono un Broker, né sono un professionista di turismo sanitario o
            dentale, sono un privato che da alcuni anni ricopre la posizione di{" "}
            <strong>consulente odontoiatrico</strong> presso la più importante
            azienda di rappresentanza ed import di materiali ed attrezzature
            destinate a dentisti e odontotecnici per il mercato Ellenico.
          </Typography>
          <Typography variant="body1" component="p">
            Questa mia pluriennale esperienza nel settore mi ha portato ad
            acquisire una profonda conoscenza sia personale, poiché{" "}
            <strong>
              quotidianamente discuto di problematiche con decine di medici
              dentisti
            </strong>
            , sia materiale perché devo costantemente rimanere aggiornato sui
            diversi materiali, sulle loro caratteristiche, sul dove e come
            utilizzarli e proporre al medico la migliore soluzione.
          </Typography>
          <Typography variant="body1" component="p">
            Per cui posso affermare di conoscere profondamente l'ambiente
            odontoiatrico e per questo motivo ho pensato che sarebbe stato utile
            mettere a disposizione di altri le mie conoscenze ed il mio
            know-how.
          </Typography>
        </Grid>
        <Grid item sm={6}></Grid>
      </Grid>
    </Section>
  );
};

export default WhoAmI;
