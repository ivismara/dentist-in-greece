import { createMuiTheme, responsiveFontSizes } from "@material-ui/core";

const Theme = createMuiTheme({
  palette: {
    type: "light",
    primary: {
      main: "#091D3E",
      light: "#7C7B96",
      dark: "#00062A",
      contrastText: "#fff",
    },
    secondary: {
      main: "#0092FF",
      contrastText: "#fff",
    },
    tertiary: {
      main: "#43D5CB",
      contrastText: "#fff",
    },
    background: {
      default: "#FEFEFF",
      paper: "#ffffff",
    },
  },
  typography: {
    fontFamily: ["Cabin", "sans-serif"].join(","),
    h1: {
      fontSize: 70,
      fontWeight: 800,
      color: "#091D3E",
    },
    h6: {
      fontSize: 18,
      fontWeight: 700,
    },
    body1: {
      fontSize: 16,
      color: "#9CA4B1",
      marginBottom: 10,
    },
    body2: {
      fontSize: 14,
      fontWeight: 700,
      color: "#091D3E",
    },
  },
  overrides: {
    MuiCard: {
      root: {
        boxShadow: "10px 10px 40px #d0d0d0",
      },
    },
  },
});
export default responsiveFontSizes(Theme);
