import React from "react";
import Layout from "../components/Layout";
import Header from "../sections/homepage/Header/Header";
import { ThemeProvider } from "@material-ui/core";
import Theme from "../theme/Theme";
import Services from "../sections/homepage/Services/Services";
import InfoCards from "../sections/homepage/InfoCards/InfoCards";
import WhyGreece from "../sections/homepage/WhyGreece/WhyGreece";
import Clinic from "../sections/homepage/Clinic/Clinic";
import ClinicServices from "../sections/homepage/ClinicServices/ClinicServices";
import Footer from "../sections/homepage/Footer/Footer";
import WhoAmI from "../sections/homepage/WhoAmI/WhoAmI";
import Contact from "../sections/homepage/Contact/Contact";

const IndexPage = () => {
  return (
    <ThemeProvider theme={Theme}>
      <Layout>
        <Header />
        <InfoCards />
        <Services />
        <WhyGreece />
        <Clinic />
        <ClinicServices />
        <WhoAmI />
        <Contact />
        <Footer />
      </Layout>
    </ThemeProvider>
  );
};

export default IndexPage;
