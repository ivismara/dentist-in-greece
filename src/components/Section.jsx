import React from "react";
import { makeStyles, useTheme } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: 60,
  },
  first: {
    padding: "90px 60px 60px 60px ",
  },
  last: {
    padding: "60px 60px 90px 60px ",
  },
}));

const Section = ({ children, dark, isFirst, isLast }) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <div
      className={
        isFirst && !isLast
          ? classes.first
          : isLast
          ? classes.last
          : classes.root
      }
      style={{ backgroundColor: dark && theme.palette.primary.main }}
    >
      {children}
    </div>
  );
};

export default Section;
