import React from "react";
import { Typography, useTheme } from "@material-ui/core";

const SectionTitle = ({ title, subtitle, light }) => {
  const theme = useTheme();
  return (
    <>
      {subtitle && (
        <Typography
          variant="h6"
          component="h4"
          style={{
            color: light
              ? theme.palette.primary.light
              : theme.palette.secondary.main,
          }}
        >
          {subtitle}
        </Typography>
      )}
      <Typography
        variant="h3"
        component="h2"
        style={{
          marginBottom: 30,
          color: light
            ? theme.palette.primary.main
            : theme.palette.primary.contrastText,
        }}
      >
        {title}
      </Typography>
    </>
  );
};

export default SectionTitle;
