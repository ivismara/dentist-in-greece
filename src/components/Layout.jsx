import React from "react";
import SEO from "./Seo";
import { AppBar, makeStyles, Typography } from "@material-ui/core";
import Logo from "../images/logo.svg";

const useStyles = makeStyles((theme) => ({
  appBar: {
    padding: "10px 60px",
    height: 60,
    display: "flex",
    justifyContent: "center",
    boxShadow: "none",
  },
  logo: {
    display: "flex",
    alignItems: "center",
  },
  logoIcon: {
    maxWidth: 30,
    marginRight: 10,
  },
  content: {
    backgroundColor: theme.palette.background.default,
  },
}));

const Layout = ({ children }) => {
  const classes = useStyles();
  return (
    <>
      <SEO />
      <div>
        <AppBar
          className={classes.appBar}
          style={{ backgroundColor: "#fff" }}
          position="static"
        >
          <div className={classes.logo}>
            <img src={Logo} className={classes.logoIcon} />
            <div>
              <Typography variant="body2" color="primary">
                DENTIST
              </Typography>
              <Typography variant="body2" color="primary">
                IN GREECE
              </Typography>
            </div>
          </div>
        </AppBar>
        <div className={classes.content}>{children}</div>
      </div>
    </>
  );
};

export default Layout;
