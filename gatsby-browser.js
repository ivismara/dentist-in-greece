import React from "react"
import { CssBaseline, ThemeProvider } from "@material-ui/core"
import Theme from "./src/theme/Theme"

export const wrapPageElement = ({ element }) => {
    return (
        <ThemeProvider theme={Theme}>
            <CssBaseline />
            {element}
        </ThemeProvider>
    )
}
