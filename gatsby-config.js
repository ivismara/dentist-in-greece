module.exports = {
  pathPrefix: `/`,
  siteMetadata: {
    title: "Dentist in Greece",
    description: "Clinica dentistica",
    url: "https://www.dentistingreece.com",
    siteUrl: "https://www.dentistingreece.com",
  },
  plugins: [
    "gatsby-plugin-sharp",
    "gatsby-plugin-sitemap",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: `gatsby-theme-material-ui`,
      options: {
        webFontsConfig: {
          fonts: {
            google: [
              {
                family: `Cabin`,
                variants: [`300`, `400`, `500`, `700`],
              },
            ],
          },
        },
      },
    },
  ],
};
